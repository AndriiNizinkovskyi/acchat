class Message < ApplicationRecord
  include Validators::Message
  include Relations::Message
  include Times

  after_create_commit { MessageBroadcastJob.perform_later(self) }
end
