module Times
  extend ActiveSupport::Concern

  included do
    def timestamp
      created_at.strftime('%H:%M:%S %d %B %Y')
    end
  end
end
