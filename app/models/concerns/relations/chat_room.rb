module Relations::ChatRoom
  extend ActiveSupport::Concern

  included do
    belongs_to :user
    has_many :messages, dependent: :destroy
  end
end
