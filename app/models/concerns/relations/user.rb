module Relations::User
  extend ActiveSupport::Concern

  included do
    has_many :chat_rooms, dependent: :destroy
    has_many :messages, dependent: :destroy
  end
end
