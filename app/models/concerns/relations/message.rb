module Relations::Message
  extend ActiveSupport::Concern

  included do
    belongs_to :user
    belongs_to :chat_room
  end
end
