module Validators::Message
  extend ActiveSupport::Concern

  included do
    validates :body, presence: true, length: { minimum: 2, maximum: 1000 }
  end
end
